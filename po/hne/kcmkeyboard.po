# translation of kcmkeyboard.po to Hindi
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Ravishankar Shrivastava <raviratlami@yahoo.com>, 2008.
# Ravishankar Shrivastava <raviratlami@aol.in>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcmkeyboard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-04 02:17+0000\n"
"PO-Revision-Date: 2009-01-23 15:19+0530\n"
"Last-Translator: Ravishankar Shrivastava <raviratlami@aol.in>\n"
"Language-Team: Hindi <kde-i18n-doc@lists.kde.org>\n"
"Language: hne\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 0.2\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "रविसंकर सिरीवास्तव, जी. करूनाकर"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "raviratlami@aol.in"

#: bindings.cpp:24
#, fuzzy, kde-format
#| msgid "KDE Keyboard Layout Switcher"
msgid "Keyboard Layout Switcher"
msgstr "केडीई कुंजीपटल खाका बदलइया"

#: bindings.cpp:26
#, kde-format
msgid "Switch to Next Keyboard Layout"
msgstr "अगले कुंजीपट खाका मं स्विच करव"

#: bindings.cpp:49
#, fuzzy, kde-format
#| msgid "Error changing keyboard layout to '%1'"
msgid "Switch keyboard layout to %1"
msgstr "कुंजीपट खाका ल '%1' मं बदले मं गलती होइस"

#: flags.cpp:77
#, kde-format
msgctxt "layout - variant"
msgid "%1 - %2"
msgstr ""

#. i18n: ectx: property (windowTitle), widget (QDialog, AddLayoutDialog)
#: kcm_add_layout_dialog.ui:14
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgid "Add Layout"
msgstr "कुंजी के दोहराव"

#. i18n: ectx: property (placeholderText), widget (QLineEdit, layoutSearchField)
#: kcm_add_layout_dialog.ui:20
#, kde-format
msgid "Search…"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, shortcutLabel)
#: kcm_add_layout_dialog.ui:45
#, kde-format
msgid "Shortcut:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, labelLabel)
#: kcm_add_layout_dialog.ui:55
#, kde-format
msgid "Label:"
msgstr "लेबल:"

#. i18n: ectx: property (text), widget (QPushButton, prevbutton)
#. i18n: ectx: property (text), widget (QPushButton, previewButton)
#: kcm_add_layout_dialog.ui:76 kcm_keyboard.ui:315
#, kde-format
msgid "Preview"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tabHardware)
#: kcm_keyboard.ui:18
#, kde-format
msgid "Hardware"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm_keyboard.ui:33
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgid "Keyboard &model:"
msgstr "कुंजी के दोहराव"

#. i18n: ectx: property (whatsThis), widget (QComboBox, keyboardModelComboBox)
#: kcm_keyboard.ui:53
#, kde-format
msgid ""
"Here you can choose a keyboard model. This setting is independent of your "
"keyboard layout and refers to the \"hardware\" model, i.e. the way your "
"keyboard is manufactured. Modern keyboards that come with your computer "
"usually have two extra keys and are referred to as \"104-key\" models, which "
"is probably what you want if you do not know what kind of keyboard you "
"have.\n"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tabLayouts)
#: kcm_keyboard.ui:94
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgid "Layouts"
msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:102
#, kde-format
msgid ""
"If you select \"Application\" or \"Window\" switching policy, changing the "
"keyboard layout will only affect the current application or window."
msgstr ""
"यदि आप मन चुनथव \"अनुपरयोग\" या \"विंडो\" स्विचिंग नीति,  कुंजीपट खाका बदले जाना "
"सिरिफ अभी हाल के अनुपरयोग या विंडो ल प्रभावित करही."

#. i18n: ectx: property (title), widget (QGroupBox, switchingPolicyGroupBox)
#: kcm_keyboard.ui:105
#, kde-format
msgid "Switching Policy"
msgstr "स्विचिंग नीति"

#. i18n: ectx: property (text), widget (QRadioButton, switchByGlobalRadioBtn)
#: kcm_keyboard.ui:111
#, kde-format
msgid "&Global"
msgstr "वैस्विक (&G)"

#. i18n: ectx: property (text), widget (QRadioButton, switchByDesktopRadioBtn)
#: kcm_keyboard.ui:124
#, kde-format
msgid "&Desktop"
msgstr "डेस्कटाप (&D)"

#. i18n: ectx: property (text), widget (QRadioButton, switchByApplicationRadioBtn)
#: kcm_keyboard.ui:134
#, kde-format
msgid "&Application"
msgstr "अनुपरयोग (&A)"

#. i18n: ectx: property (text), widget (QRadioButton, switchByWindowRadioBtn)
#: kcm_keyboard.ui:144
#, kde-format
msgid "&Window"
msgstr "विंडो (&W)"

#. i18n: ectx: property (title), widget (QGroupBox, shortcutsGroupBox)
#: kcm_keyboard.ui:157
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgid "Shortcuts for Switching Layout"
msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm_keyboard.ui:163
#, kde-format
msgid "Main shortcuts:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkbGrpShortcutBtn)
#: kcm_keyboard.ui:176
#, kde-format
msgid ""
"This is a shortcut for switching layouts which is handled by X.org. It "
"allows modifier-only shortcuts."
msgstr ""
"ए ह खाका ल स्विच करे के सार्टकट हे, जऊन ल X.org हेंडल करथे. ए सिरिफ-माडीफायर सार्टकट "
"ल स्वीकारथे."

#. i18n: ectx: property (text), widget (QPushButton, xkbGrpShortcutBtn)
#. i18n: ectx: property (text), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:179 kcm_keyboard.ui:209
#, kde-format
msgctxt "no shortcut defined"
msgid "None"
msgstr ""

#. i18n: ectx: property (text), widget (QToolButton, xkbGrpClearBtn)
#. i18n: ectx: property (text), widget (QToolButton, xkb3rdLevelClearBtn)
#: kcm_keyboard.ui:186 kcm_keyboard.ui:216
#, kde-format
msgid "…"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm_keyboard.ui:193
#, kde-format
msgid "3rd level shortcuts:"
msgstr "३रा स्तर सार्टकट:"

#. i18n: ectx: property (whatsThis), widget (QPushButton, xkb3rdLevelShortcutBtn)
#: kcm_keyboard.ui:206
#, kde-format
msgid ""
"This is a shortcut for switching to a third level of the active layout (if "
"it has one) which is handled by X.org. It allows modifier-only shortcuts."
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm_keyboard.ui:223
#, kde-format
msgid "Alternative shortcut:"
msgstr "वैकल्पिक सार्टकट:"

#. i18n: ectx: property (whatsThis), widget (KKeySequenceWidget, kdeKeySequence)
#: kcm_keyboard.ui:236
#, kde-format
msgid ""
"This is a shortcut for switching layouts. It does not support modifier-only "
"shortcuts and also may not work in some situations (e.g. if popup is active "
"or from screensaver)."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, kcfg_configureLayouts)
#: kcm_keyboard.ui:261
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgid "Configure layouts"
msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#. i18n: ectx: property (text), widget (QPushButton, addLayoutBtn)
#: kcm_keyboard.ui:275
#, kde-format
msgid "Add"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, removeLayoutBtn)
#: kcm_keyboard.ui:285
#, kde-format
msgid "Remove"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, moveUpBtn)
#: kcm_keyboard.ui:295
#, kde-format
msgid "Move Up"
msgstr ""

#. i18n: ectx: property (text), widget (QPushButton, moveDownBtn)
#: kcm_keyboard.ui:305
#, kde-format
msgid "Move Down"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, layoutLoopingCheckBox)
#: kcm_keyboard.ui:350
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgid "Spare layouts"
msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm_keyboard.ui:382
#, kde-format
msgid "Main layout count:"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tabAdvanced)
#: kcm_keyboard.ui:412
#, kde-format
msgid "Advanced"
msgstr "विस्तृत"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_resetOldXkbOptions)
#: kcm_keyboard.ui:418
#, fuzzy, kde-format
#| msgid "&Enable keyboard repeat"
msgid "&Configure keyboard options"
msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#: kcm_keyboard_widget.cpp:204
#, kde-format
msgctxt "unknown keyboard model vendor"
msgid "Unknown"
msgstr ""

#: kcm_keyboard_widget.cpp:206
#, kde-format
msgctxt "vendor | keyboard model"
msgid "%1 | %2"
msgstr ""

#: kcm_keyboard_widget.cpp:633
#, fuzzy, kde-format
#| msgid "None"
msgctxt "no shortcuts defined"
msgid "None"
msgstr "कुछ नइ"

#: kcm_keyboard_widget.cpp:647
#, kde-format
msgid "%1 shortcut"
msgid_plural "%1 shortcuts"
msgstr[0] ""
msgstr[1] ""

#: kcm_view_models.cpp:200
#, fuzzy, kde-format
#| msgid "Map"
msgctxt "layout map name"
msgid "Map"
msgstr "मैप"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Layout"
msgstr "खाका"

#: kcm_view_models.cpp:200
#, kde-format
msgid "Variant"
msgstr "वेरियंट"

#: kcm_view_models.cpp:200
#, fuzzy, kde-format
msgid "Label"
msgstr "लेबल:"

#: kcm_view_models.cpp:200
#, fuzzy, kde-format
#| msgid "Main shortcuts:"
msgid "Shortcut"
msgstr "मुख्य सार्टकट:"

#: kcm_view_models.cpp:273
#, fuzzy, kde-format
#| msgctxt "Default variant"
#| msgid "Default"
msgctxt "variant"
msgid "Default"
msgstr "डिफाल्ट"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcmmiscwidget.ui:31
#, kde-format
msgid "When a key is held:"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, accentMenuRadioButton)
#: kcmmiscwidget.ui:38
#, kde-format
msgid "&Show accented and similar characters "
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, repeatRadioButton)
#: kcmmiscwidget.ui:45
#, kde-format
msgid "&Repeat the key"
msgstr ""

#. i18n: ectx: property (text), widget (QRadioButton, nothingRadioButton)
#: kcmmiscwidget.ui:52
#, kde-format
msgid "&Do nothing"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcmmiscwidget.ui:66
#, kde-format
msgid "Test area:"
msgstr ""

#. i18n: ectx: property (toolTip), widget (QLineEdit, lineEdit)
#: kcmmiscwidget.ui:73
#, kde-format
msgid ""
"Allows to test keyboard repeat and click volume (just don't forget to apply "
"the changes)."
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:82
#, kde-format
msgid ""
"If supported, this option allows you to setup the state of NumLock after "
"Plasma startup.<p>You can configure NumLock to be turned on or off, or "
"configure Plasma not to set NumLock state."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, numlockGroupBox)
#: kcmmiscwidget.ui:85
#, fuzzy, kde-format
#| msgid "NumLock on KDE Startup"
msgid "NumLock on Plasma Startup"
msgstr "केडीई चालू होय के बेरा मं न्यूम-लाक"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton1)
#: kcmmiscwidget.ui:97
#, kde-format
msgid "T&urn on"
msgstr "चालू करव (&u)"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton2)
#: kcmmiscwidget.ui:104
#, fuzzy, kde-format
#| msgid "Turn o&ff"
msgid "&Turn off"
msgstr "बन्द करव (&f)"

#. i18n: ectx: property (text), widget (QRadioButton, radioButton3)
#: kcmmiscwidget.ui:111
#, kde-format
msgid "Leave unchan&ged"
msgstr "बिन बदले छोड़ देव (&g)"

#. i18n: ectx: property (text), widget (QLabel, lblRate)
#: kcmmiscwidget.ui:148
#, kde-format
msgid "&Rate:"
msgstr "दरः (&R)"

#. i18n: ectx: property (whatsThis), widget (QSlider, delaySlider)
#. i18n: ectx: property (whatsThis), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:164 kcmmiscwidget.ui:202
#, fuzzy, kde-format
#| msgid ""
#| "If supported, this option allows you to set the rate at which keycodes "
#| "are generated while a key is pressed."
msgid ""
"If supported, this option allows you to set the delay after which a pressed "
"key will start generating keycodes. The 'Repeat rate' option controls the "
"frequency of these keycodes."
msgstr ""
"यदि समर्थित होही, तहां ए विकल्प ह आप मन ल जब कुंजी पट दबाय जाही, तहां कऊन दर से की-"
"कोड जेनरेट होही, एला सेट करन देथे ."

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, kcfg_repeatRate)
#. i18n: ectx: property (whatsThis), widget (QSlider, rateSlider)
#: kcmmiscwidget.ui:192 kcmmiscwidget.ui:212
#, kde-format
msgid ""
"If supported, this option allows you to set the rate at which keycodes are "
"generated while a key is pressed."
msgstr ""
"यदि समर्थित होही, तहां ए विकल्प ह आप मन ल जब कुंजी पट दबाय जाही, तहां कऊन दर से की-"
"कोड जेनरेट होही, एला सेट करन देथे ."

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, kcfg_repeatRate)
#: kcmmiscwidget.ui:195
#, kde-format
msgid " repeats/s"
msgstr ""

#. i18n: ectx: property (suffix), widget (QSpinBox, kcfg_repeatDelay)
#: kcmmiscwidget.ui:205
#, kde-format
msgid " ms"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, lblDelay)
#: kcmmiscwidget.ui:246
#, kde-format
msgid "&Delay:"
msgstr "देरीः (&D)"

#: tastenbrett/main.cpp:52
#, fuzzy, kde-format
#| msgid "Keyboard Repeat"
msgctxt "app display name"
msgid "Keyboard Preview"
msgstr "कुंजी के दोहराव"

#: tastenbrett/main.cpp:54
#, fuzzy, kde-format
#| msgid "KDE Keyboard Layout Switcher"
msgctxt "app description"
msgid "Keyboard layout visualization"
msgstr "केडीई कुंजीपटल खाका बदलइया"

#: tastenbrett/main.cpp:139
#, kde-format
msgctxt "@label"
msgid ""
"The keyboard geometry failed to load. This often indicates that the selected "
"model does not support a specific layout or layout variant. This problem "
"will likely also present when you try to use this combination of model, "
"layout and variant."
msgstr ""

#, fuzzy
#~| msgid "KDE Keyboard Layout Switcher"
#~ msgid "KDE Keyboard Control Module"
#~ msgstr "केडीई कुंजीपटल खाका बदलइया"

#, fuzzy
#~| msgid "Copyright (C) 2006-2007 Andriy Rysin"
#~ msgid "(c) 2010 Andriy Rysin"
#~ msgstr "कापीराइट (c) 2006-2007 आन्द्रेई राइसिन"

#, fuzzy
#~| msgid "KDE Keyboard Layout Switcher"
#~ msgid "KDE Keyboard Layout Switcher"
#~ msgstr "केडीई कुंजीपटल खाका बदलइया"

#, fuzzy
#~| msgid "&Enable keyboard repeat"
#~ msgid "Show for single layout"
#~ msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#, fuzzy
#~| msgid "Keyboard Repeat"
#~ msgctxt "tooltip title"
#~ msgid "Keyboard Layout"
#~ msgstr "कुंजी के दोहराव"

#, fuzzy
#~| msgid "&Enable keyboard repeat"
#~ msgid "Configure Layouts..."
#~ msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#~ msgid "Keyboard Repeat"
#~ msgstr "कुंजी के दोहराव"

#~ msgid "Turn o&ff"
#~ msgstr "बन्द करव (&f)"

#, fuzzy
#~| msgid "Leave unchan&ged"
#~ msgid "&Leave unchanged"
#~ msgstr "बिन बदले छोड़ देव (&g)"

#, fuzzy
#~| msgid "&Enable keyboard repeat"
#~ msgid "Configure..."
#~ msgstr "कुंजी के दोहराव सक्छम करव (&E)"

#, fuzzy
#~| msgid "Key click &volume:"
#~ msgid "&Key click volume:"
#~ msgstr "कुंजी किलिक अवाजः (&v)"

#~ msgid "&Enable keyboard repeat"
#~ msgstr "कुंजी के दोहराव सक्छम करव (&E)"
