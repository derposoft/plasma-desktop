# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Elkana Bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.kickoff\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-16 01:59+0000\n"
"PO-Revision-Date: 2017-05-22 05:18-0400\n"
"Last-Translator: Elkana Bardugo <ttv200@gmail.com>\n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Zanata 3.9.6\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr ""

#: package/contents/ui/code/tools.js:49
#, kde-format
msgid "Remove from Favorites"
msgstr "הסר ממועדפים"

#: package/contents/ui/code/tools.js:53
#, kde-format
msgid "Add to Favorites"
msgstr "הוסף למועדפים"

#: package/contents/ui/code/tools.js:77
#, kde-format
msgid "On All Activities"
msgstr ""

#: package/contents/ui/code/tools.js:127
#, kde-format
msgid "On the Current Activity"
msgstr ""

#: package/contents/ui/code/tools.js:141
#, fuzzy, kde-format
#| msgid "Favorites"
msgid "Show in Favorites"
msgstr "מועדפים"

#: package/contents/ui/ConfigGeneral.qml:39
#, kde-format
msgid "Icon:"
msgstr "סמל:"

#: package/contents/ui/ConfigGeneral.qml:45
#, kde-format
msgctxt "@action:button"
msgid "Change Application Launcher's icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:46
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Current icon is %1. Click to open menu to change the current icon or reset "
"to the default icon."
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:50
#, kde-format
msgctxt "@info:tooltip"
msgid "Icon name is \"%1\""
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:83
#, fuzzy, kde-format
#| msgctxt "@item:inmenu Open icon chooser dialog"
#| msgid "Choose..."
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "בחר..."

#: package/contents/ui/ConfigGeneral.qml:85
#, kde-format
msgctxt "@info:whatsthis"
msgid "Choose an icon for Application Launcher"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:89
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:95
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:106
#, kde-format
msgctxt "@label:textbox"
msgid "Text label:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:108
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to add a text label"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:123
#, kde-format
msgctxt "@action:button"
msgid "Reset menu label"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:137
#, kde-format
msgctxt "@info"
msgid "A text label cannot be set when the Panel is vertical."
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:148
#, kde-format
msgctxt "General options"
msgid "General:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:149
#, fuzzy, kde-format
#| msgid "Sort alphabetically"
msgid "Always sort applications alphabetically"
msgstr "סדר לפי שם"

#: package/contents/ui/ConfigGeneral.qml:154
#, kde-format
msgid "Use compact list item style"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:160
#, kde-format
msgctxt "@info:usagetip under a checkbox when Touch Mode is on"
msgid "Automatically disabled when in Touch Mode"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:169
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:179
#, fuzzy, kde-format
#| msgid "Favorites"
msgid "Show favorites:"
msgstr "מועדפים"

#: package/contents/ui/ConfigGeneral.qml:180
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a grid'"
msgid "In a grid"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:188
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a list'"
msgid "In a list"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:196
#, fuzzy, kde-format
#| msgid "Show applications by name"
msgid "Show other applications:"
msgstr "הראה יישומים על ידי שמות"

#: package/contents/ui/ConfigGeneral.qml:197
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a grid'"
msgid "In a grid"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:205
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a list'"
msgid "In a list"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:217
#, kde-format
msgid "Show buttons for:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:218
#: package/contents/ui/LeaveButtons.qml:115
#, kde-format
msgid "Power"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:227
#, kde-format
msgid "Session"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:236
#, kde-format
msgid "Power and session"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:245
#, kde-format
msgid "Show action button captions"
msgstr ""

#: package/contents/ui/Footer.qml:98
#, kde-format
msgid "Applications"
msgstr "יישומים"

#: package/contents/ui/Footer.qml:111
#, kde-format
msgid "Places"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:144
#, kde-format
msgctxt "@info:status"
msgid "No matches"
msgstr ""

#: package/contents/ui/Header.qml:64
#, kde-format
msgid "Open user settings"
msgstr ""

#: package/contents/ui/Header.qml:217
#, kde-format
msgid "Keep Open"
msgstr ""

#: package/contents/ui/Kickoff.qml:304
#, fuzzy, kde-format
#| msgid "Edit Applications..."
msgid "Edit Applications…"
msgstr "ערוך יישומים..."

#: package/contents/ui/KickoffGridView.qml:89
#, kde-format
msgid "Grid with %1 rows, %2 columns"
msgstr ""

#: package/contents/ui/LeaveButtons.qml:115
#, kde-format
msgid "Leave"
msgstr "יציאה"

#: package/contents/ui/LeaveButtons.qml:115
#, kde-format
msgid "More"
msgstr ""

#: package/contents/ui/PlacesPage.qml:48
#, fuzzy, kde-format
#| msgid "Computer"
msgctxt "category in Places sidebar"
msgid "Computer"
msgstr "מחשב"

#: package/contents/ui/PlacesPage.qml:49
#, fuzzy, kde-format
#| msgid "History"
msgctxt "category in Places sidebar"
msgid "History"
msgstr "היסטוריה"

#: package/contents/ui/PlacesPage.qml:50
#, kde-format
msgctxt "category in Places sidebar"
msgid "Frequently Used"
msgstr ""

#~ msgctxt "@item:inmenu Reset icon to default"
#~ msgid "Clear Icon"
#~ msgstr "נקה סמל"

#, fuzzy
#~| msgctxt "Type is a verb here, not a noun"
#~| msgid "Type to search..."
#~ msgid "Search…"
#~ msgstr "הקלד כדי לחפש..."

#, fuzzy
#~| msgid "Leave"
#~ msgid "Leave…"
#~ msgstr "יציאה"

#, fuzzy
#~| msgid "Edit Applications..."
#~ msgid "Updating applications…"
#~ msgstr "ערוך יישומים..."

#~ msgid "%2@%3 (%1)"
#~ msgstr "%2@%3 (%1)"

#~ msgid "%1@%2"
#~ msgstr "%1@%2"

#, fuzzy
#~| msgid "Leave"
#~ msgid "Leave..."
#~ msgstr "יציאה"

#, fuzzy
#~| msgctxt "@item:inmenu Open icon chooser dialog"
#~| msgid "Choose..."
#~ msgid "More..."
#~ msgstr "בחר..."

#~ msgid "Applications updated."
#~ msgstr "היישומים מעודכנים."

#~ msgid "Switch tabs on hover"
#~ msgstr "החלף כרטיסיה בריחוף"

#~ msgid "All Applications"
#~ msgstr "כל היישומים"

#~ msgid "Favorites"
#~ msgstr "מועדפים"

#~ msgid "Often Used"
#~ msgstr "בשימוש לעיתים קרובות"

#, fuzzy
#~| msgid "Hidden Tabs"
#~ msgid "Active Tabs"
#~ msgstr "כרטיסיות נסתרות"

#~ msgid ""
#~ "Drag tabs between the boxes to show/hide them, or reorder the visible "
#~ "tabs by dragging."
#~ msgstr ""
#~ "גרור כרטיסיות בין התיבות כדי להציג או להסתיר אותן, או סדר מחדש את "
#~ "הכרטיסיות הגלויות על ידי גרירה."

#~ msgid "Expand search to bookmarks, files and emails"
#~ msgstr "הרחב חיפוש ל־סמניות, קבצים והודעות דואר אלקטרוני"

#~ msgid "Appearance"
#~ msgstr "מראה"

#~ msgid "Menu Buttons"
#~ msgstr "כפתורי תפריט"

#~ msgid "Visible Tabs"
#~ msgstr "כרטיסיות גלויות"
